open SVFGraph
open Llvm
open Containers


include PAG_intf

type pag_id = int

module PAGEdge = struct

  type t = pag_edge

  let compare = Stdlib.compare
  let default = {instruction=(const_int (() |> global_context |> i8_type) 1) ;assign_type=AddrPE}
end


type node = {value: llvalue option; val_type: Llvm.lltype option; node_type: node_ty;id:pag_id;}



module PAGNode = struct 
  type t = node
  let get_id nd = nd.id
end


include Make(PAGNode)(PAGEdge)


let create_node m_value nd_ty val_ty id = {value=m_value; node_type=nd_ty;val_type=val_ty;id=id}

module PAGId = struct 
  type t = pag_id
  let compare = compare
  let pp = Format.pp_print_int
end

(*let example_graph = add_vertex empty {value=None;node_type=Obj;pag_id=1}*)
let int_from_id  x = x
let id_from_int x = x 


