open Containers

type pts_to_info = (Set.Make(PAG.PAGId).t ) Map.Make(PAG.PAGId).t

type t = {pag: PAG.t;icfg: ICFG.t; pts: pts_to_info}


val build_model: string -> t

val build_pag: SVFffi.pag_t -> PAG.t
val build_icfg: SVFffi.pag_t -> ICFG.t
val build_pts_to: SVFffi.flowres_t -> pts_to_info

val pp_pts_to: Format.formatter -> pts_to_info -> unit

val get_entry_points: t -> ICFG.node list