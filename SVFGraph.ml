open Graph

module type SVFEdge = Graph.Sig.ORDERED_TYPE_DFT
module type SVFNode = sig 
  type t 
  val get_id: t -> int
end

module MarkedNode (ExtNode:SVFNode) = struct 
  type t = ExtNode.t
  let compare x y = compare (ExtNode.get_id x) (ExtNode.get_id y)
  let hash = ExtNode.get_id 
  let equal x y = ExtNode.get_id x = ExtNode.get_id y
end

module Make (Vertex: SVFNode) (Edge:SVFEdge) = struct
  module GNode = MarkedNode(Vertex)
  include    Persistent.Digraph.ConcreteBidirectionalLabeled(GNode)(Edge)
end 