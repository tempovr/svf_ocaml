open Containers
open Llvm

module PAGIdSet = Set.Make(PAG.PAGId)
module PAGIdMap = Map.Make(PAG.PAGId)

type pts_to_info = PAGIdSet.t PAGIdMap.t 

type t = {pag: PAG.t;icfg: ICFG.t; pts: pts_to_info}


module P = SVFffi.PAG

let pag_node_to_vert nd = PAG.create_node (SVFffi.PAGNode.get_value nd) (SVFffi.PAGNode.get_type nd) (SVFffi.PAGNode.get_val_type nd) (SVFffi.PAGNode.get_id nd)

let add_nodes nds ~translate_node ~add_to_graph ~graph = nds |> (List.map translate_node) |> (List.fold_left add_to_graph graph)


let translate_edge_pag edg = (((edg |> SVFffi.PAGEdge.get_src |> pag_node_to_vert ),
                           ({instruction=(const_int (() |> global_context |> i8_type) 1); assign_type=(SVFffi.PAGEdge.get_type edg)}:PAG.pag_edge),
                           (edg |> SVFffi.PAGEdge.get_dst |> pag_node_to_vert)):PAG.edge)

let add_edges grph nds ~add_edge_to_graph ~get_edges ~translate_edge = nds |> List.map get_edges |> (List.fold_left (@) []) |> List.map translate_edge |> List.fold_left add_edge_to_graph grph

let build_pag pg = let pag_list = P.get_list pg in let nodes = add_nodes ~translate_node:pag_node_to_vert ~add_to_graph:PAG.add_vertex ~graph: PAG.empty pag_list in 
  let full = add_edges ~add_edge_to_graph:PAG.add_edge_e ~get_edges:SVFffi.PAGNode.get_out_edges ~translate_edge:translate_edge_pag nodes pag_list in full

let icfgt_to_node  = function 
    `FunCallBlock nd -> print_endline "call block";ICFG.CallBlock {actual_params=(SVFffi.CallBlock.get_args nd |> List.map pag_node_to_vert);cs=SVFffi.CallBlock.get_cs nd}
  | `FunEntryBlock nd -> print_endline "fun entry"; ICFG.FunEntry (SVFffi.FunEntryBlock.get_params nd |> List.map pag_node_to_vert)
  | `FunExitBlock nd ->print_endline "fun exit"; ICFG.FunExit (SVFffi.FunExitBlock.get_formal_ret nd |> (function | None -> None
  | Some x -> Some (pag_node_to_vert x)))
  | `FunRetBlock nd -> print_endline "ret block";ICFG.RetBlock {actual_ret=(SVFffi.RetBlock.get_actual_ret nd |> (Option.map pag_node_to_vert));cs=(SVFffi.RetBlock.get_cs nd)}
  | `IntraBlock nd ->print_endline "intrablock"; ICFG.IntraBlock ((SVFffi.IntraBlockNode.get_insn nd), (nd |> SVFffi.IntraBlockNode.get_pag_edges |> (List.map translate_edge_pag)) )



let icfg_node_to_vert nd = let ty = SVFffi.ICFGNode.get_type nd in
  let () = print_endline "getting id" in
  let nd_id = SVFffi.ICFGNode.get_id nd in
  let () = print_endline "got id" in
  let func = SVFffi.ICFGNode.get_function nd in
  let () = print_endline "got func" in 
  ICFG.create_node  {func=func;block=(icfgt_to_node ty)} nd_id


let translate_edge_icfg edg = SVFffi.ICFGEdge.get_src edg |> SVFffi.ICFGNode.get_id |> string_of_int |> print_endline;(SVFffi.ICFGEdge.get_src edg |> icfg_node_to_vert, SVFffi.ICFGEdge.get_type edg, SVFffi.ICFGEdge.get_dst edg|> icfg_node_to_vert)
let build_icfg pg = 
  let icfgt = SVFffi.ICFG.get pg in 
  let icfg_nodes = SVFffi.ICFG.get_list icfgt 
  in let nodes = print_endline "starting nodes";add_nodes icfg_nodes ~translate_node:icfg_node_to_vert ~add_to_graph:ICFG.add_vertex ~graph:ICFG.empty in
  print_endline "done with nodes";add_edges ~add_edge_to_graph:ICFG.add_edge_e ~get_edges:(fun nd -> print_endline (string_of_int (SVFffi.ICFGNode.get_id nd)^"getting edges"); let res = SVFffi.ICFGNode.get_out_edges nd in print_endline "done";res) ~translate_edge:translate_edge_icfg nodes icfg_nodes


let build_pts_to flowres = let pag = SVFffi.PAG.get flowres |> build_pag in 
  let assoc = PAG.fold_vertex (fun nd curr_list -> (nd.id,SVFffi.FlowSensitive.get_pts_to flowres nd.id |> PAGIdSet.of_list) ::curr_list) pag []   in  (PAGIdMap.of_list assoc)


let pp_pts_to = (PAGIdMap.pp PAG.PAGId.pp (PAGIdSet.pp PAG.PAGId.pp))

let build_model filename =
  let model = let newptr = Ctypes.allocate Ctypes.string filename in SVFffi.SVFModule.build_svf_module newptr 1 in
  let fpta = SVFffi.SVFModule.create_flowsensitive_results model in 
  let ffi_pag = SVFffi.PAG.get fpta in 
  let pag = build_pag ffi_pag in 
  let pts_to = build_pts_to fpta in
  let () = print_endline"done with pts" in
  let icfg = build_icfg ffi_pag in  {icfg=icfg;pag=pag;pts=pts_to}





let get_entry_points (m:t) = ICFG.fold_vertex (fun nd tot -> if ICFG.in_degree m.icfg nd = 0 then nd::tot else tot)  m.icfg []
