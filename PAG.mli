open SVFGraph
open Llvm

include module type of PAG_intf

type pag_id

type node = {value: llvalue option; val_type: Llvm.lltype option;node_type: node_ty;id:pag_id;}


module PAGNode : SVFNode with type t=node
module PAGEdge : SVFEdge with type t=pag_edge

val create_node: llvalue option -> node_ty -> Llvm.lltype option ->int -> PAGNode.t

include module type of Make(PAGNode)(PAGEdge)

module PAGId: sig 
  include Map.OrderedType with type t = pag_id
  val pp: Format.formatter -> pag_id -> unit
end

val int_from_id: pag_id -> int
val id_from_int: int -> pag_id