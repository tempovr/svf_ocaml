open Llvm


type callsite = llvalue

type icfg_edge = IntraCF 
               | CallCF of callsite
               | RetCF of callsite
type call_info = {actual_params:PAG.node list; cs:callsite}
type ret_info = {actual_ret:PAG.node option;cs:callsite}


type block_info = IntraBlock of llvalue * PAG.edge list 
| FunEntry of PAG.node list
               | FunExit of PAG.node option | CallBlock of call_info | RetBlock of ret_info

type icfg_node = {func: llvalue;block: block_info}


