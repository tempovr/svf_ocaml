open Graph

module type SVFEdge = Graph.Sig.ORDERED_TYPE_DFT
module type SVFNode = sig 
  type t
  val get_id: t -> int 
end


module Make  (Vertex: SVFNode) (Edge: SVFEdge) :
sig 
  include Sig.P with type V.t = Vertex.t and type E.t=(Vertex.t*Edge.t*Vertex.t)
end