open SVFGraph

include ICFG_intf

type node_id = int

type node = {id: node_id; data: icfg_node }


module ICFGNode = struct
  type t = node
 (** let compare = compare*)
  let get_id x = x.id 
end


module ICFGEdge = struct 
  type t = icfg_edge
  let default = IntraCF
  let compare = compare
end


let create_node nd id  = {id=id;data=nd}

include Make(ICFGNode)(ICFGEdge)

let nd_id_to_string id = string_of_int id