
open SVFGraph



include module type of ICFG_intf


type node_id

type node = {id: node_id; data: icfg_node }

module ICFGNode : sig 
  include SVFNode with type t = node 
  (*val compare: t -> t -> int*)
end 
module ICFGEdge : SVFEdge with type t = icfg_edge

val create_node: icfg_node -> int -> node

val nd_id_to_string: node_id -> string


include module type of Make(ICFGNode)(ICFGEdge)