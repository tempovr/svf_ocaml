

type edge_ty =  AddrPE | CallPE | CopyPE | GepPE | LoadPE | RetPE | StorePE | TDForkPE | TDJoinPE | CmpPE | BinaryOpPE
type pag_edge={instruction: Llvm.llvalue ; assign_type: edge_ty}

type node_ty = Obj | Ret | Val | VarArg [@@deriving sexp_of]