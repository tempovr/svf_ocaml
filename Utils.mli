

val (+=): ('a -> 'b) -> ('c -> 'a) -> ('c -> 'b)
module Dot: sig 
  include module type of Graph.Graphviz.Dot(
    struct 
      include ICFG
      let edge_attributes (a, e, b) = []
      let default_edge_attributes _ = []
      let get_subgraph _ = None
      let vertex_attributes _ = [`Shape `Box]
      let vertex_name v = ICFG.nd_id_to_string v.id
      let default_vertex_attributes _ = []
     let graph_attributes _ = []
    end
    )
end