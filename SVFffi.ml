open Ctypes
open Foreign
open Utils


type svfmodule_t = unit Ctypes_static.ptr
type flowres_t = unit Ctypes_static.ptr
type pag_t = unit Ctypes_static.ptr
type pag_node_t = unit Ctypes_static.ptr
type pag_edge_t = unit Ctypes_static.ptr
type icfg_t = unit Ctypes_static.ptr
type icfg_node_t = unit Ctypes_static.ptr
type icfg_edge_t = unit Ctypes_static.ptr


type funcall_block_t = unit Ctypes_static.ptr
type funentry_block_t = unit Ctypes_static.ptr
type funexit_block_t = unit Ctypes_static.ptr
type funret_block_t = unit Ctypes_static.ptr
type intra_block_t = unit Ctypes_static.ptr
type memobj_t = unit Ctypes_static.ptr


let memobj_t = (ptr void)

let svfmodule_t = (ptr void)
let flowres_t = (ptr void)
let pag_t = (ptr void)
let pag_node_t = (ptr void)
let pag_node_iter = (ptr void)
let pag_edge_iter = (ptr void)
let pag_edge_t = (ptr void)
let icfg_node_t = (ptr void)
let icfg_t = (ptr void)
let icfg_iter_t = (ptr void)
let icfg_pag_nd_iter_t = (ptr void)
let icfg_pag_edge_iter_t = (ptr void) 

let nodeid_iter_t = (ptr void)

let funcall_block_t = (ptr void)
let funentry_block_t = (ptr void)
let funexit_block_t = (ptr void)
let funret_block_t = (ptr void)

let icfg_edge_iter_t = (ptr void)

let intra_block_t = (ptr void)

let icfg_edge_t = (ptr void)

module SVFModule = struct


  let build_svf_module = foreign "build_svf_module" ( ptr string   @-> int @-> returning svfmodule_t)
  let create_flowsensitive_results = foreign "create_flowsensitive_results" (svfmodule_t  @-> returning flowres_t)
end

let build_get_iter get_iter  end_check get_curr get_next from = 
  let curr_iter = get_iter from in
  let rec after _ = if end_check curr_iter then Seq.Nil
    else Seq.Cons (get_curr curr_iter, (fun _ -> get_next curr_iter;after ())) in (after, curr_iter)

let iter_to_list iter iterptr destroyer = let res = iter |> Seq.fold_left (fun l i -> i::l) [] |> List.rev in destroyer iterptr; res

let build_get_list get_iter  end_check get_curr get_next destroyer from = let (it, ptr) = build_get_iter get_iter  end_check get_curr get_next from in iter_to_list it ptr destroyer


module FlowSensitive = struct 
  let get_iter = foreign "get_points_to_iter" (flowres_t @-> int @-> returning nodeid_iter_t)
  let iter_curr = foreign "nodeid_iter_curr" (nodeid_iter_t @-> returning uint32_t)
  let iter_end = foreign "nodeid_iter_end" (nodeid_iter_t @-> returning bool)
  let iter_next = foreign "nodeid_iter_next" (nodeid_iter_t @-> returning void)

  let iter_destroy = foreign "nodeid_iter_destroy" (nodeid_iter_t @-> returning void)

  let get_pts_to_internal fres = build_get_list (get_iter fres) iter_end iter_curr iter_next iter_destroy

  let get_pts_to fres = List.map (PAG.id_from_int += Unsigned.UInt32.to_int) += (get_pts_to_internal fres) += PAG.int_from_id  
  let destroy = foreign "destroy_flowsensitive_results" (flowres_t @-> returning void)
end

module P=PAG



module PAG = struct 
  let get = foreign "pag_get" (pag_t @-> returning pag_t)
  let internal_get_iter = foreign "pag_iter" (pag_node_t @-> returning pag_node_iter)
  let internal_iter_end = foreign "pag_iter_end" (pag_node_iter @-> returning bool)
  let internal_get_curr = foreign "pag_iter_curr" (pag_node_iter @-> returning pag_node_t)
  let internal_next = foreign "pag_iter_next" (pag_node_iter @-> returning void)
  let internal_destroy_iter = foreign "pag_iter_destroy" (pag_node_iter @-> returning void)
  let get_list = build_get_list internal_get_iter internal_iter_end internal_get_curr internal_next internal_destroy_iter
end


module MemObj = struct 
  external get_lltype_internal: nativeint -> Llvm.lltype = "memobj_get_ty"
  let has_type = foreign  "memobj_has_ty" (memobj_t @-> returning bool)
  let get_lltype nd = if has_type nd then Some( get_lltype_internal (Ctypes.raw_address_of_ptr nd)) else None
end
module PAGEdge = struct 
  let destroy = foreign "pag_edge_destroy" (pag_edge_t @-> returning void)
  let get_type_internal = foreign "pag_edge_type" (pag_edge_t @-> returning int)
  let to_enum_val = function
    | 0 -> P.AddrPE
    | 1 -> P.CopyPE
    | 2 -> P.StorePE
    | 3 -> P.LoadPE
    | 4 -> P.CallPE
    | 5 -> P.RetPE
    | 6 -> P.GepPE
    | 7 -> P.GepPE
    | 8 -> P.TDForkPE
    | 9 -> P.TDJoinPE
    | 10 -> P.CmpPE
    | 11 -> P.BinaryOpPE
    | _ -> raise (Invalid_argument "Received invalid enum member: edge type")
  let get_type = to_enum_val += get_type_internal
  let get_src = foreign "pag_edge_src" (pag_edge_t @-> returning pag_node_t)
  let get_dst = foreign "pag_edge_dst" (pag_edge_t @-> returning pag_node_t)
end

module PAGNode = struct
  let internal_has_value = foreign "pag_has_value" (pag_node_t @-> returning bool)
  external internal_get_value: nativeint -> Llvm.llvalue = "pag_get_value_caml"
  let get_value x = if internal_has_value x then Some (internal_get_value (Ctypes.raw_address_of_ptr x)) else None


 
  let get_id_internal = foreign "pag_node_id" (pag_node_t @-> returning uint64_t)
  let get_id = Unsigned.UInt64.to_int += get_id_internal 
  let out_edges_internal = foreign "pag_out_edges" (pag_node_t @-> returning pag_edge_iter)
  let iter_end_internal = foreign "pag_edge_iter_end" (pag_node_iter @-> returning bool )
  let iter_curr_internal = foreign "pag_edge_curr" (pag_edge_iter @-> returning pag_edge_t)
  let iter_next_internal = foreign "pag_edge_next" (pag_edge_iter @-> returning void)
  let iter_destroy_internal = foreign "pag_edge_iter_destroy" (pag_edge_iter @-> returning void)
  let get_out_edges = build_get_list out_edges_internal iter_end_internal iter_curr_internal iter_next_internal iter_destroy_internal

  let to_enum_val x =  match x with
    | 0 -> P.Val
    | 1 -> P.Obj
    | 2 -> P.Ret
    | 3 -> P.VarArg
    | 4 -> P.Val
    | 5 -> P.Obj
    | 6 -> P.Obj
    | 7 -> P.Val
    | 8 -> P.Obj
    | _ -> raise (Invalid_argument "Received invalid enum member: node type")
  let get_type_internal = foreign "pag_node_type" (pag_node_t @-> returning int)

  let get_type nd  = nd |> get_type_internal |> to_enum_val

  let destroy = foreign "pag_node_destroy" (pag_node_t @-> returning void)

  let get_memobj = foreign "pag_node_get_memobj" (pag_node_t @-> returning memobj_t)

  let get_val_type nd = match get_type nd with 
    | P.Obj -> get_memobj nd |> MemObj.get_lltype
    | _ -> if internal_has_value nd then Some(internal_get_value (Ctypes.raw_address_of_ptr nd)|> Llvm.type_of) else None
end
module ICFGRepr = ICFG

module ICFGEdge =  struct 

  let kind_to_enum = function 
  | 0 -> `IntraCF
  | 1 -> `CallCF
  | 2 -> `RetCF
  | _ -> raise (Invalid_argument "invalid enum kind: icfg_edge type")
  let get_kind_internal = foreign "icfg_edge_getkind" (icfg_edge_t @-> returning int)

  external ret_callsite_internal: nativeint -> Llvm.llvalue = "icfg_ret_edge_callsite"
  external call_callsite_internal: nativeint -> Llvm.llvalue = "icfg_call_edge_callsite"
  let get_ret_callsite x = ret_callsite_internal (Ctypes.raw_address_of_ptr x)

  let get_call_callsite x = call_callsite_internal (Ctypes.raw_address_of_ptr x)
  let get_type nd = let kind = get_kind_internal nd |> kind_to_enum in match kind with 
    | `IntraCF -> ICFG.IntraCF
    | `CallCF -> ICFG.CallCF (get_call_callsite nd)
    | `RetCF -> ICFG.RetCF  (get_ret_callsite nd)
  let get_src = foreign "icfg_edge_src" (icfg_edge_t @-> returning pag_node_t)
  let get_dst = foreign "icfg_edge_dst" (icfg_edge_t @-> returning pag_node_t)
end


module ICFG = struct
  let get_iter_internal = foreign "get_icfg_iter" (icfg_t @-> returning icfg_iter_t)
  let iter_end_internal = foreign "icfg_iter_end" (icfg_iter_t @-> returning bool )
  let iter_curr_internal = foreign "icfg_curr" (icfg_iter_t @-> returning icfg_node_t)
  let iter_next_internal = foreign "icfg_next" (icfg_iter_t @-> returning void)
  let iter_destroy_internal = foreign "icfg_iter_destroy" (icfg_iter_t @-> returning void)

  let iter_end_debug x = let res = iter_end_internal x in res

  let get_list_internal = build_get_list get_iter_internal iter_end_debug iter_curr_internal iter_next_internal iter_destroy_internal
  let get_list x = let res = get_list_internal x in res
  let get = foreign "get_icfg" (pag_t @-> returning icfg_t)
end

module ICFGNode = struct
  let get_iter_internal = foreign "get_out_edge_iter" (icfg_node_t @-> returning icfg_edge_iter_t)
  let iter_end_internal = foreign "icfg_edge_iter_end" (icfg_edge_iter_t @-> returning bool )
  let iter_curr_internal = foreign "icfg_edge_curr" (icfg_edge_iter_t @-> returning icfg_edge_t)
  let iter_next_internal = foreign "icfg_edge_next" (icfg_edge_iter_t @-> returning void)
  let iter_destroy_internal = foreign "icfg_edge_iter_destroy" (icfg_iter_t @-> returning void)

  let iter_end_debug x = let res =  iter_end_internal x in ("Is end: "^string_of_bool res)|> print_endline;res

  let get_list_internal = build_get_list get_iter_internal iter_end_debug iter_curr_internal iter_next_internal iter_destroy_internal

  let get_out_edges = get_list_internal
  external get_function_internal: nativeint -> Llvm.llvalue = "icfg_node_get_function"
  let get_function x =  let func = (get_function_internal (Ctypes.raw_address_of_ptr x)) in func
  let get_id_internal = foreign "icfg_node_id" (icfg_node_t @-> returning uint64_t)
  let get_type_internal = foreign "get_icfg_node_ty" (icfg_node_t @-> returning int)

  let get_type nd = (function | 0 -> `IntraBlock  nd
                              | 1 -> `FunEntryBlock  nd
                              | 2 -> `FunExitBlock  nd
                              | 3 -> `FunCallBlock  nd
                              | 4 -> `FunRetBlock  nd
                              | _ -> raise (Invalid_argument "Received invalid enum member: icfg node type")) (get_type_internal nd)
  let get_id =  Unsigned.UInt64.to_int += get_id_internal
end




module ICFGPagIter = struct
  let nd_iter_end= foreign "icfg_pag_nd_iter_end" (icfg_pag_nd_iter_t @-> returning bool)

  let nd_iter_curr = foreign "icfg_pag_nd_iter_curr" (icfg_pag_nd_iter_t @-> returning pag_node_t)

  let nd_iter_destroy = foreign "icfg_pag_nd_iter_destroy" (icfg_pag_nd_iter_t @-> returning void)

  let edge_iter_curr = foreign "icfg_pag_edge_iter_curr" (icfg_pag_edge_iter_t @-> returning pag_edge_t)

  let nd_iter_next = foreign "icfg_pag_nd_iter_next" (icfg_pag_nd_iter_t @-> returning void)

  let edge_iter_next = foreign "icfg_pag_edge_iter_next" (icfg_pag_edge_iter_t @-> returning void)

  let edge_iter_end = foreign "icfg_pag_edge_iter_end" (icfg_pag_edge_iter_t @-> returning bool)

  let edge_iter_destroy = foreign "icfg_pag_edge_iter_destroy" (icfg_pag_edge_iter_t @-> returning void)
end


let get_pag_node_list f = build_get_list f ICFGPagIter.nd_iter_end ICFGPagIter.nd_iter_curr ICFGPagIter.nd_iter_next ICFGPagIter.nd_iter_destroy

module IntraBlockNode = struct 
 (* let get_insn_internal = foreign "icfg_intra_insn" (intra_block_t @-> returning (ptr void))
  let get_insn x = get_insn_internal x |> Obj.magic*)
  external get_insn_internal: nativeint -> Llvm.llvalue = "icfg_intra_insn"
  let get_insn x = let addr = raw_address_of_ptr x in print_endline (Nativeint.to_string addr); get_insn_internal addr
  let pag_edges_internal = foreign "icfg_intra_edges" (intra_block_t @-> returning pag_edge_iter)

  let get_pag_edges =  build_get_list pag_edges_internal ICFGPagIter.edge_iter_end ICFGPagIter.edge_iter_curr ICFGPagIter.edge_iter_next ICFGPagIter.edge_iter_destroy

  let print_addr = foreign "icfg_intra_addrprint" (intra_block_t @-> returning void)
end

module FunEntryBlock = struct
  let params_internal = foreign "icfg_funentry_params" (funentry_block_t @-> returning pag_node_iter)
  let get_params = get_pag_node_list params_internal
end

module RetBlock = struct 
  let get_cs_internal = foreign "icfg_ret_getcallsite" (funcall_block_t @-> returning (ptr void))
  external get_cs_internal: nativeint -> Llvm.llvalue = "icfg_ret_getcallsite"
  let get_cs x = Ctypes.raw_address_of_ptr x |> get_cs_internal
  let get_actual_ret_internal = foreign "icfg_ret_actual" (funret_block_t @-> returning pag_node_t)
  let get_actual_ret x = let res = get_actual_ret_internal x in if is_null res then None else Some res 
end

module CallBlock = struct
  let args_internal = foreign "icfg_callblock_params" (funcall_block_t @-> returning pag_node_iter)
  let get_args = get_pag_node_list args_internal
  external get_cs_internal: nativeint -> Llvm.llvalue = "icfg_call_getcallsite"
  let get_cs x = get_cs_internal (Ctypes.raw_address_of_ptr x)
end

module FunExitBlock = struct 
  let get_formal_ret_internal = foreign "icfg_funexit_ret" (funexit_block_t @-> returning pag_node_t)
  let get_formal_ret  x = let res = (get_formal_ret_internal x) in if is_null res then None else Some res
end