type svfmodule_t 
type flowres_t
type pag_t
type pag_node_t
type pag_edge_t
type icfg_t
type icfg_node_t

type funcall_block_t
type funentry_block_t
type funexit_block_t
type funret_block_t
type intra_block_t
type icfg_edge_t

module SVFModule:  sig 
  val build_svf_module: string Ctypes_static.ptr -> int -> svfmodule_t
  val create_flowsensitive_results: svfmodule_t -> flowres_t
end


module FlowSensitive: sig 
  val get_pts_to: flowres_t -> PAG.pag_id -> PAG.pag_id list
  val destroy: flowres_t -> unit
end

module PAGEdge: sig 
  val destroy: pag_edge_t -> unit
  val get_type: pag_edge_t -> PAG.edge_ty
  val get_src: pag_edge_t -> pag_node_t
  val get_dst: pag_edge_t -> pag_node_t
end

module PAGNode: sig
  val get_id: pag_node_t -> int
  val destroy: pag_node_t -> unit
  val get_type: pag_node_t -> PAG.node_ty
  val get_out_edges: pag_node_t -> pag_edge_t list
  val get_value: pag_node_t -> Llvm.llvalue option
  val get_val_type: pag_node_t -> Llvm.lltype option
end


module PAG: sig 
  val get: flowres_t -> pag_t
  val get_list: pag_t -> pag_node_t list
end

module ICFGRepr = ICFG

module ICFGEdge: sig 
  val get_type: icfg_edge_t -> ICFG.icfg_edge
  val get_src: icfg_edge_t -> icfg_node_t
  val get_dst: icfg_edge_t -> icfg_node_t
end

module ICFG: sig
  val get_list: icfg_t -> icfg_node_t list
  val get: pag_t -> icfg_t
end




module ICFGNode: sig
  val get_type: icfg_node_t -> [> `FunCallBlock of funcall_block_t| `FunEntryBlock of funentry_block_t| `FunExitBlock of funexit_block_t| `FunRetBlock of funret_block_t| `IntraBlock of intra_block_t]
  val get_id: icfg_node_t -> int
  val get_function: icfg_node_t -> Llvm.llvalue
  val get_out_edges: icfg_node_t -> icfg_edge_t list 
end



module FunEntryBlock: sig
  val get_params: funentry_block_t -> pag_node_t list
end

module FunExitBlock: sig
  val get_formal_ret: funexit_block_t -> pag_node_t option
end

module IntraBlockNode: sig
  val get_insn: intra_block_t -> Llvm.llvalue
  val get_pag_edges: intra_block_t -> pag_edge_t list
  val print_addr: intra_block_t -> unit
end

module CallBlock: sig 
  val get_args: funcall_block_t -> pag_node_t list
  val get_cs: funcall_block_t -> Llvm.llvalue
end

module RetBlock: sig
  val get_actual_ret: funret_block_t -> pag_node_t option
  val get_cs: funret_block_t -> Llvm.llvalue
end