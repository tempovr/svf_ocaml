open OUnit2
open Svf_ocaml.SVFffi


let mods = let newptr = Ctypes.allocate Ctypes.string "/home/ian/Documents/svf_analysis/lib/svf_ocaml/svf_c/examples/simple_loop.ll" in  (SVFModule.build_svf_module newptr 1) 

let fpta = 
  SVFModule.create_flowsensitive_results  mods



let test_sum _ = let pag = PAG.get fpta in 
  let pag_iter = PAG.get_list pag in
  let sum = (List.fold_left (fun sum nd -> ((nd |> PAGNode.get_value |> ignore);PAGNode.get_id nd) + sum) 0 pag_iter) in
  let () = List.iter PAGNode.destroy pag_iter in
  assert_equal ~printer:string_of_int sum  3160


(*let test_build_icfg  = let pag = PAG.get fpta in let _ = Svf_ocaml.SVFModel.build_icfg pag in assert_bool "is true" true
*)


let icfg_iter_test = 
  let pag = PAG.get fpta in 
  let icfg = ICFG.get pag in ICFG.get_list icfg |> List.map (ICFGNode.get_function)

let pts_to_test = Svf_ocaml.SVFModel.build_pts_to fpta


let sexp_pts = Svf_ocaml.SVFModel.pp_pts_to Format.std_formatter pts_to_test

(*let icfg_iter_test = let pag = PAG.get fpta in Svf_ocaml.SVFModel.build_icfg pag*)
let suite = "FFI tests" >::: ["test_sum_of_pag_ids" >:: test_sum]


let () = run_test_tt_main suite; FlowSensitive.destroy fpta